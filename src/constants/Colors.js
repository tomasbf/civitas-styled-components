const tintColorLight = "#2f95dc";
const tintColorDark = "#fff";

const Colors =  {
  light: {
    text: "#000",
    background: "#fff",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: "#fff",
    background: "#000",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
  },
  light_green: {
    text: "#000",
    background: "#62760C",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
    lighter: "#66ff66",
    pressed: "#000",
    notPressed: "#fff",
  },
  dark_green: {
    text: "#fff",
    background: "#535204",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark
  },
  yellow: {
    text: "#0C31B5",
    background: "#FBB836",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
  },
  light_yellow: {
    text: "#0C31B5",
    background: "#CDB30C",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
    lighter: "#ffff99",
    pressed: "#0C31B5",
    notPressed: "#000",

  },
  brown: {
    text: "#fff",
    background: "#523906",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark
  }


};
export default Colors;
