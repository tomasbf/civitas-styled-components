import { Text } from 'react-native';
import React from 'react';
import Header from "../../components/Header";
import SearchBar from '../../components/SearchBar';
import Background from '../../components/Background';
import TitleSection from '../../components/TitleSection';
import ItemBox from '../../components/ItemBox';
import Item from '../../components/Item';
import Button from '../../components/Button';



const Home = ({navigation}) => {
  return (
    <Background>
      <Header navigation={navigation} />
      <SearchBar />
      <ItemBox
        title="Serviços"
        children={[
          <Item key="1" title="Souza e Silva" description="Contratando engenheiros para obra" image="services/obra.jpg" />,
          <Item key="2" title="Salão da Cláudia" description="Cortes de cabelo por apenas 50 reais" image="services/salao_beleza.png" />,
          <Item key="3" title="Bozo" description="Animo festas infantis" image="services/palhaço.jpg" />,
          <Item key="4" title="Sokka Viagens" description="Passeios para a Ilha do Fogo" image="services/passeio_barco.jpg" />,
        ]}
      />
      <ItemBox
        title="Produtos" 
        children={[
          <Item key="1" title="Horta da Fruta" image="products/alface.jpg" description="Alface" />,
          <Item key="2" title="Assentamento Pablo Neruda" image="products/mandioca.jpg" description="Mandioca orgânica 100%, saudemos!" />,
          <Item key="3" title="Electric Dream" image="products/carroça.jpg" description="Carro 100% carbon neutral, versátil e rápido" />,
          
        ]}
        />
      <Button>Anunciar</Button>
    </Background>
  );
};
export default Home;
