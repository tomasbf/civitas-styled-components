import * as React from 'react';
import { Logo, Container } from './styles.js';
import { useEffect } from 'react';


const Initial = ({ navigation }) => {
    const image = require("../../assets/images/white_logo_civitas.png");

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('Home');
        }, 5000);
    }, []);

    return (
        <Container>
            <Logo source={image} />
        </Container>
    );

};
export default Initial;

