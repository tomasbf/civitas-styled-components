import styled from 'styled-components/native';
import Colors from "../../constants/Colors.js";

import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;


export const Logo = styled.Image`

    height: 300px;
    width: 300px;
    transform: translateY(-20px);
    

`;


export const Container = styled.View`
    
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    justify-content: center;
    background-color: ${Colors.light.background};

    height: ${ScreenHeight}px;
    width: ${ScreenWidth}px;

`;
