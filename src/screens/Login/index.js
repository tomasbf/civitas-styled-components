import LoginBox from '../../components/LoginBox/index.js';

import Background from '../../components/Background';
import Header from '../../components/Header/index.js';
import CadastrarButton from '../../components/CadastrarButton/index.js';
const Login = (props) => {

    return (
        <Background>
            <Header 
                navigation={props.navigation} 
                profileVisible={false}  
                title="Entrar"    
            />

            <CadastrarButton navigation={props.navigation}/>
            <LoginBox navigation={props.navigation}/>
        </Background>
        
    );

};

export default Login;