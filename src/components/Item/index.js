import { ItemStyled, ItemText, ItemTitle, ItemImage, ItemTextView } from "./styles";
import { useState } from "react";

const Item = (props) => {

    const [translate, setTranslate] = useState(0);
    let yEnd = 0;
    let heightTitle = 0;
    let yAbs = 0;

    let image;
    try {
        image = require("../../assets/uploads/" + props.image);
    } catch (error) {
        image = require("../../assets/images/no-pictures.png");
    }

    return (
        <ItemStyled>
            <ItemTextView
                translate={0}
                onLayout={(event) => {
                    yAbs = event.nativeEvent.layout.y;
                    heightTitle = event.nativeEvent.layout.height;

                }}

            >   
                <ItemTitle>{props.title}</ItemTitle>
            </ItemTextView>
            <ItemImage 
                onLayout={(event) => {
                    yEnd = event.nativeEvent.layout.y + event.nativeEvent.layout.height;
                }}
                source={image} />
            <ItemTextView

                translate={translate}

                onLayout={(event) => {
                    const {x, y, height, width} = event.nativeEvent.layout;
                    
                    // 150 é a altura máxima do quadrado
                    if((yEnd - yAbs) + height > 150) {
                        setTranslate(150 - (yEnd - yAbs) - height);
                    }
                    
                }}
            >
                <ItemText>{props.description}</ItemText>
            </ItemTextView>
        </ItemStyled>

    );

};
export default Item;