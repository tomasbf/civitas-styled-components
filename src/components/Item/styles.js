import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

export const ItemStyled = styled.View`

    margin: 0px 15px 0px 15px;

    width: 150px;
    height: 150px;
    max-height: 150px;
    border-radius: 10px;
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
    justify-content: space-between;
    overflow-block: hidden;

    background-color: ${Colors.light.background};
`;
export const ItemText = styled.Text`
    
    font-size: 16px;
    font-family: 'Hubballi';
    color: ${Colors.light.text};
    margin-top: 2px;
    padding: 5px;
    background-color: ${Colors.light.background};
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    width: 150px;
`;
export const ItemTitle = styled.Text`
    
    margin-top: 5px;
    width: 150px;
    margin-bottom: 5px;
    font-size: 20px;
    font-weight: bold;
    font-family: 'Hubballi';
    color: ${Colors.light.text};
    text-align: center;

    `;

export const ItemTextView = styled.View`
    height: fit-content;
    width: fit-content;
    transform: translateY(${(props) => props.translate}px);
`;

export const ItemImage = styled.Image`

    height: 80px;
    width: 150px;

`;