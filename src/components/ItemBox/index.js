import { ItemBoxStyled } from "./styles";
import TitleSection from "../TitleSection";
import ItemBar from "../ItemBar";
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

const ItemBox = (props) => {
    
    return (
        <ItemBoxStyled>
            <TitleSection>{props.title}</TitleSection>
            <ItemBar 
                    sizeOfBar={ScreenWidth - 80}
                    children={props.children}
            />
        </ItemBoxStyled>
    );


};

export default ItemBox;