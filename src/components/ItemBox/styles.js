import styled from 'styled-components/native';
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

export const ItemBoxStyled = styled.View`
    display: flex;
    flex-direction: column wrap;
    justify-content: center;
    align-items: flex-start;
    padding: 20px;
    width: fit-content;
    max-width: ${ScreenWidth}px;
    margin: 10px;

    background-color: transparent;

    align-self: center;
    
`;

