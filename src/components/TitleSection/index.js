import { TitleSectionStyle } from "./styles";
import React from 'react';

const TitleSection = ({children}) => {

    return (
        <TitleSectionStyle>
            {children}
        </TitleSectionStyle>
    );

};

export default TitleSection;