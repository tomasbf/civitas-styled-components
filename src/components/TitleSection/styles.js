import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

export const TitleSectionStyle = styled.Text`
    margin-bottom: 15px;
    font-size: 35px;
    color: ${Colors.brown.text};
    font-family: Hubballi;
    width: fit-content;
`;
