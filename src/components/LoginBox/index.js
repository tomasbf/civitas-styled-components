import { Box } from './styles';
import Input from '../Input';
import Link from '../Link';
import Button from '../Button';
import Colors from '../../constants/Colors';
import { View } from 'react-native';

import ButtonGoogle from '../ButtonGoogle';

import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;


const LoginBox = (props) => {

    return (
        <Box>
            <Input label="Email" />
            <Input label="Senha" secureTextEntry={true} />
            <Link
                page="Home"
                navigation={props.navigation}
            >Esqueceu a senha?</Link>
            <View
                style={{ marginTop: 30 }}
            >
                <Button

                    navigation={props.navigation}
                    borderRadius={10}
                    size={0.7}
                    color={Colors.light_yellow}
                    width={ScreenWidth * 0.6}
                >
                    Entrar
                </Button>
            </View>
            <View
                style={{ marginTop: 30 }}
            >
                <ButtonGoogle />
                <ButtonGoogle googleOrFacebook="facebook" />
            </View>
        </Box>


    );


};

export default LoginBox;