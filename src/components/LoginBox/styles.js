import styled from 'styled-components/native';
import Colors from '../../constants/Colors';
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;


export const Box = styled.View`

    background-color: ${Colors.dark_green.background};
    width: ${ScreenWidth * 0.8}px;
    height: ${ScreenHeight * 0.6}px;

    border-radius: 30px;
    padding: 30px;

    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;

`;