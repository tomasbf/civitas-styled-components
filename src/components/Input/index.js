import { Container, InputStyled, TextStyled } from './styles';
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

const Input = ({width=ScreenWidth * 0.6, ...props}) => {

    return (

        <Container>

            <TextStyled>{props.label}</TextStyled>
            <InputStyled width={width} {...props} />

        </Container>
        

    );

};
export default Input;