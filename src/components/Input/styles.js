import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

export const TextStyled = styled.Text`
    color: ${Colors.dark.text};
    font-family: 'Hubballi';
    font-size: 20px;

`;

export const InputStyled = styled.TextInput`

    width: ${props => props.width}px;

    margin-top: 10px;

    border-radius: 10px;
    padding: 10px;
    font-size: 20px;
    font-family: 'Hubballi';

    background-color: ${Colors.light.background};
    color: ${Colors.light.text};


`;

export const Container = styled.View`

    background-color: transparent;

    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: space-between;

    width: fit-content;
    height: fit-content;

    margin-top: 15px;


`;

