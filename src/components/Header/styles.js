import styled from 'styled-components/native';
import Colors from "../../constants/Colors.js";
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;


const width = ScreenWidth;

export const Logo = styled.Image`

    height: 80px;
    width: 90px;
    margin: 15px;
    transform: translateY(-5px);

`;
export const ProfilePhoto = styled.Image`
    height: 80px;
    width: 90px;
`;
export const ProfilePhotoContainer = styled.View`
    height: 80px;
    width: 90px;
    margin: 15px;
`;

export const Title = styled.Text`
    weight: bold;
    font-family: 'Hubballi';
    font-size: 50px;
    color: ${Colors.yellow.text};

`;

export const Container = styled.View`

    padding-top: 20px;
    display: flex;
    width: ${width}px;

    flex-flow: row nowrap;
    align-items: center;
    
    justify-content: space-between;


    background-color: ${Colors.yellow.background};
`;
