import React from 'react';
import { View } from 'react-native';
import { Logo, Container, Title, ProfilePhoto, ProfilePhotoContainer } from './styles.js';
const Header = ({ navigation, profileVisible = true, title="Civitas" }) => {

    const logo = require('../../assets/images/yellow_logo_civitas.png');
    const profilePhoto = require('../../assets/images/profile_photo.png');


    return (
        <Container>
            <Logo source={logo} />
            <Title>{title}</Title>
            {profileVisible ?
                <ProfilePhotoContainer
                    onTouchEnd={() => {
                        navigation.navigate('Login');
                    }}>
                    <ProfilePhoto
                        source={profilePhoto}
                    />
                </ProfilePhotoContainer>
                :
                <ProfilePhotoContainer></ProfilePhotoContainer>
            }
        </Container>
    );


};
export default Header;