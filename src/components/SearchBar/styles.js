import styled from 'styled-components/native';
import Colors from '../../constants/Colors';
import { Dimensions } from "react-native";
import Feather from 'react-native-vector-icons/Feather';

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

export const SearchIconVisible = styled(Feather)`
    color: ${Colors.light.text};
    transform: translateX(30px);
`;
export const SearchIconHidden = styled(Feather)`
    color: transparent;
`;

export const FilterIcon = styled(Feather)`

    color: ${Colors.light.text};
    z-index: 2;
    transform: translateX(-30px);


`;



export const SearchInput = styled.TextInput`

    border-radius: 10px;
    font-family: Hubballi;
    background-color: ${Colors.light.background};
    padding: 10px 40px 10px 40px;
    font-size: 20px;

`;

export const Container = styled.View`
    margin-top: 30px;
    background-color: transparent;
    display: flex;
    flex-flow: row nowrap;
    width: ${ScreenWidth}px;
    justify-content: center;
    align-items: center;


`;


