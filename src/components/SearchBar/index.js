import React from 'react';
import { SearchIconVisible, SearchIconHidden, Container, SearchInput, FilterIcon } from './styles';
import { useState } from 'react';



const SearchBar = () => {
    const [icon, setIcon] = useState(true);
    const searchIconVisible = <SearchIconVisible size={25} name="search" />;
    const searchIconHidden = <SearchIconHidden size={25} name="search" />;

    return (
        <Container>
            {icon? searchIconVisible : searchIconHidden}
            <SearchInput 
                style={{paddingLeft: icon? 35 : 10}}
                onChangeText={(text) => {
                    setIcon(!(text.length > 0));
                }}
                placeholder={"Buscar"}
                autoCorrect={false}
            />
            <FilterIcon size={25} name="sliders" />
        </Container>
       
    );


};

export default SearchBar;