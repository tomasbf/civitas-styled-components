import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

export const Wrapper = styled.View`
    background-color: transparent;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    margin-bottom: 30px;
    margin-top: 30px;

`;

export const Text = styled.Text`

    font-family: 'Hubballi';
    margin: ${props => 20 * props.size}px;
    font-size: ${props => props.size * 28}px;
    color: ${Colors.brown.text};
`;