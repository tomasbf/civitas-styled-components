import Button from "../Button";
import { Wrapper, Text } from "./styles";
const CadastrarButton = ({ navigation }) => {
    return (
        <Wrapper>
            <Text size={0.5} >Não tem conta?</Text>
            <Button
                navigation={navigation}
                page="Home"
                size={0.5}
            >Cadastre-se</Button>
        </Wrapper>

    );
};

export default CadastrarButton;