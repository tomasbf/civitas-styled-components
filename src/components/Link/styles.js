import styled from 'styled-components';
import Colors from '../../constants/Colors';

export const LinkStyled = styled.a`

    color: ${Colors.dark.text};
    font-family: 'Hubballi';
    font-size: 15px;
    margin-top: 10px;
    text-decoration: underline;
    

`;