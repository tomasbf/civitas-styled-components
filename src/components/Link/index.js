import { LinkStyled } from './styles';

const Link = ({children, navigation, page}) => {

    return (

        <LinkStyled onTouchEnd={() => {
            navigation.navigate(page);
        }}>
            {children}
        </LinkStyled>

    );

};
export default Link;