import styled from 'styled-components/native';
import Colors from '../../constants/Colors';

import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

export const BackgroundColored = styled.View`
    background-color: ${Colors.brown.background};
    width: ${ScreenWidth}px;
    height: ${ScreenHeight}px;


    display: flex;
    felx-flow: column nowrap;
    align-items: center;
    
`;