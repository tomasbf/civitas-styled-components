import React from 'react';
import { BackgroundColored } from './styles';

const Background = ({children}) => {

    return (
        <BackgroundColored>
            {children}
        </BackgroundColored>
    );

};

export default Background;