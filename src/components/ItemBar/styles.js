import styled from 'styled-components/native';


export const ItemBarStyled = styled.ScrollView`

    background-color: transparent;
    max-width: ${(props) => props.sizeOfBar}px;

`;