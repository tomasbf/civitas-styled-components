import { ItemBarStyled } from "./styles";
import {Text} from 'react-native';

const ItemBar = (props) => {

    return (
        <ItemBarStyled sizeOfBar={props.sizeOfBar} horizontal={true}>
            {props.children}
        </ItemBarStyled>
    );

};
export default ItemBar;