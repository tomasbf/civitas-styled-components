import styled from "styled-components/native";
import Colors from '../../constants/Colors';

import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

export const ButtonStyled = styled.TouchableOpacity`

    activeOpacity: 1;
    width: ${ScreenWidth * 0.6}px;
    padding: 10px 15px 10px 15px;
    border-radius: 10px;
    background-color: ${Colors.light.background};

    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    margin-bottom: 15px;


`;

export const Logo = styled.Image`

    width: 20px;
    height: 20px;

`;