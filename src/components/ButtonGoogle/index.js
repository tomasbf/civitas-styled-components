import { ButtonStyled, Logo } from './styles';

const ButtonGoogle = ({googleOrFacebook="google"}) => {

    return (

        <ButtonStyled>
            <Logo 
                source={googleOrFacebook === "google" ? require('../../assets/images/google.png') : require('../../assets/images/facebook.png')} />
        </ButtonStyled>

    );
    

};

export default ButtonGoogle;