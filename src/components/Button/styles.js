import styled from 'styled-components/native';
import Colors from '../../constants/Colors';


export const ButtonStyled = styled.TouchableOpacity`
    background-color: ${props => props.pressed ? props.color.lighter : props.color.background };
    border-radius: ${props=> props.borderRadius}px;
    padding: ${props => 15 * props.size}px;
    text-align: center;

    width: ${props => props.width}px;

`;

export const Text = styled.Text`
    font-family: 'Hubballi';
    color: ${props => props.pressed ? props.color.pressed : props.color.notPressed};
    font-size: ${props => 28 * props.size}px;
`;
