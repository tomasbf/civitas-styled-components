import { ButtonStyled } from './styles';
import { useState } from 'react';
import { Text } from './styles';
import Colors from '../../constants/Colors';
import { Dimensions } from "react-native";

const ScreenWidth = Dimensions.get('screen').width;
const ScreenHeight = Dimensions.get('screen').height;

const Button = ({children, borderRadius=30,color=Colors.light_green, size=1, width=ScreenWidth * 0.8 * size, navigation=null, page='Home'}) => {


    const [pressed, setPressed] = useState(false);

    return (

        <ButtonStyled
            width={width}
            borderRadius={borderRadius}
            color={color}
            activeOpacity={1}
            size={size} 
            onPressIn={() => setPressed(true)}
            onPressOut={() => setPressed(false)}
            onPress={() => navigation !== null ? navigation.navigate(page) : null}
            onBlur={() => setPressed(false)}
            pressed={pressed}
            >
            <Text
                color={color}
                pressed={pressed}
                size={size}
            >{children}</Text>
        </ButtonStyled>

    );


};

export default Button;